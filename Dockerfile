FROM node:12.16.1 
WORKDIR /balerina/server
COPY . .
RUN npm install
EXPOSE 8081
CMD ["node", "./src/app.js"]