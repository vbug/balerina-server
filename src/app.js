require('dotenv').config();
const express = require('express')
const postgres = require('./config/postgres')
const app = express();
const configExpress = require('./config/express')

const port = process.env.SERVER_PORT;

configExpress(app)

  postgres.sync()
  .then(()=>{
    console.log('synced!')
    app.listen(port,()=>{
      console.log(`listening on port ${port}...`)
  })
  })
  .catch((err)=>{console.log('error '+ err)})