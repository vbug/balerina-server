const launchCountryRouter = require('../../api/launchCountry');
const messageRouter = require('../../api/message');
const realityRouter = require('../../api/reality');
const tbmKindRouter = require('../../api/tbmKind');
const threatRouter = require('../../api/threat');

module.exports = (app) =>{
    app.use('/launchCountry',launchCountryRouter);
    app.use('/message',messageRouter);
    app.use('/reality',realityRouter);
    app.use('/tbmKind',tbmKindRouter);
    app.use('/threat',threatRouter);
}