const bodyParser = require('body-parser');
const cors = require('cors')
const setRoutes = require('./routes')

module.exports = (app) => {
app.use(bodyParser.json()); 
app.use(cors());
setRoutes(app)
}