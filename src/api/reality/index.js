const router = require("express-async-router").AsyncRouter();
const controller = require("./reality.controller")

router.get('/getRealities', controller.getRealities);

module.exports = router;