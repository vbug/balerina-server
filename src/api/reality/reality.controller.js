let reality = require('./reality.model')

    module.exports.getRealities = async(req, res) => {
      try{
      let response = await reality.findAll();
      res.json(response)
    } 
    catch(err){
      console.log(err)
    }
  }
