const postgres = require('../../config/postgres')
const Sequelize = require('sequelize');

let reality = postgres.define('hafifa_reality', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
  }
}, {
  timestamps: false
});

module.exports = reality