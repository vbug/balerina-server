const router = require("express-async-router").AsyncRouter();
const controller = require("./launchCountry.controller")

router.get('/getCountries', controller.getLaunchCountries);

module.exports = router;
