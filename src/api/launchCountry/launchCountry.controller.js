let launchCountry = require('./launchCountry.model')

  module.exports.getLaunchCountries = async (req, res) => {
    try {
      console.log(req)
        let response = await launchCountry.findAll()
        res.json(response)
      }
      catch (err) {
        console.log(err)
      }
  }