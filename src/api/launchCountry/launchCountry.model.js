// let postgres = require('/balerina/server/src/config/postgres')
const postgres = require('../../config/postgres')
const Sequelize = require('sequelize');

let launchCountry = postgres.define('hafifa_launch_country', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    name: {
        allowNull: false,
        type: Sequelize.STRING,
    }
  }, {
    timestamps: false
  });

  module.exports = launchCountry;