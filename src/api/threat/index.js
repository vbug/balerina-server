const router = require("express-async-router").AsyncRouter();
const controller = require("./threat.controller")

router.get('/getThreats', controller.getThreats);
router.post('/getThreatById', controller.getThreatById);
router.post('/deleteThreat', controller.deleteThreat);
router.post('/updateThreat', controller.updateThreat);
router.post('/createThreat', controller.createThreat);

module.exports = router;