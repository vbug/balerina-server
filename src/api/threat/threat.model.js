const postgres = require('../../config/postgres')
let Sequelize = require('sequelize');
let reality = require('../reality/reality.model')
let message = require('../message/message.model')

let threat = postgres.define('hafifa_threat', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    name: {
        allowNull: false,
        type: Sequelize.STRING,
    },
}, {
    createdAt: false

});

threat.belongsTo(reality, { as: 'reality', foreignKey: 'realityId' })
threat.hasMany(message, { as: 'messages', foreignKey: 'threatId', onDelete: 'cascade' })

module.exports = threat