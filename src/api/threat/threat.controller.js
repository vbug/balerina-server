let threat = require('./threat.model')
let reality = require('../reality/reality.model')
let message = require('../message/message.model')
let tbmKind = require('../tbmKind/tbmKind.model')
let launchCountry = require('../launchCountry/launchCountry.model')

module.exports.getThreats = async (req, res) => {
    try {
        let response = await threat.findAll({
            include: [{ model: reality, as: 'reality' },
            {
                model: message, as: 'messages', include: [
                    { model: tbmKind, as: 'tbmKind' },
                    { model: launchCountry, as: 'launchCountry' }
                ]
            }]
        });
        res.json(response);
    }
    catch (err) {
        console.log(err);
    }
}

module.exports.getThreatById = async (req, res) => {
    try {
        let response = await threat.findOne({
            where: {
                id: req.body.id
            },
            include: [{ model: reality, as: 'reality' },
            {
                model: message, as: 'messages', include: [
                    { model: tbmKind, as: 'tbmKind' },
                    { model: launchCountry, as: 'launchCountry' }
                ]
            }],
            order: [
                [{ model: message, as: 'messages' }, 'id', 'ASC'],
            ],

        });
        res.json(response);
    }
    catch (err) {
        console.log(err);
    }
}

module.exports.deleteThreat = async (req, res) => {
    try {
        threat.destroy({
            where: {
                id: req.body.id
            }
        })
    }
    catch (err) {
        console.log(err);
    }
}

module.exports.updateThreat = async (req, res) => {
    try {
        await threat.update(req.body, {
            where: {
                id: req.body.id
            }
        });
    }
    catch (err) {
        console.log(err)
    }
}

module.exports.createThreat = async (req, res) => {
    try {
        await threat.create(req.body)
    }
    catch (err) {
        console.log(err)
    }
}