let message = require('./message.model')

module.exports.createMessage = async (req, res) => {
  try {
    await message.create(req.body)
  }
  catch (err) {
    console.log(err)
  }
}

module.exports.deleteMessage = async (req, res) => {
  try {
    message.destroy({
      where: {
        id: req.body.id
      }
    })
  }
  catch (err) {
    console.log(err)
  }
}

module.exports.updateMessage = async (req, res) => {
  try {
    await message.update(req.body, {
      where: {
        id: req.body.id
      }
    });
  }
  catch (err) {
    console.log(err)
  }
}