const postgres = require('../../config/postgres')
const Sequelize = require('sequelize');
const launchCountry = require('../launchCountry/launchCountry.model')
const tbmKind = require('../tbmKind/tbmKind.model')

let message =  postgres.define('hafifa_message', {
    id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER
    },
    delayFromStart: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    timeUntilImpact: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    launchX: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    launchY: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    impactX: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    impactY: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    elipseWidth: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
    elipseLength: {
        allowNull: false,
        type: Sequelize.INTEGER,
    },
  }, {});

  message.belongsTo(launchCountry,{foreignKey: 'launchCountryId', as:'launchCountry'});
  message.belongsTo(tbmKind,{foreignKey: 'tbmKindId', as:'tbmKind'});

  module.exports = message