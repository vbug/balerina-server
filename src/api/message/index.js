const router = require("express-async-router").AsyncRouter();
const controller = require("./message.controller");

router.post('/createMessage', controller.createMessage );
router.post('/deleteMessage', controller.deleteMessage);
router.post('/updateMessage', controller.updateMessage);

module.exports = router;