const router = require("express-async-router").AsyncRouter();
const controller = require("./tbmKind.controller")

router.get('/getTbmKinds', controller.getTbmKinds);

module.exports = router;