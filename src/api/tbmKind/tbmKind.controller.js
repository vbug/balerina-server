let tbmKind = require('./tbmKind.model')

module.exports.getTbmKinds =  async(req, res) => {
      try{
      let response = await tbmKind.findAll();
      res.json(response)
      }
      catch(err){
        console.log(err)
      }
    }