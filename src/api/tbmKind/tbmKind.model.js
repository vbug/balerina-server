const postgres = require('../../config/postgres')
const Sequelize = require('sequelize');

let tbmKind =  postgres.define('hafifa_tbm_kind', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    name: {
        allowNull: false,
        type: Sequelize.STRING,
    }
  }, {
    timestamps: false
  });

  module.exports = tbmKind;